# frozen_string_literal: true

require 'test_helper'

class PortTest < ActiveSupport::TestCase
  test 'port has a number before saving' do
    p = Port.new
    assert_not p.save, 'Saved the port without a number'
  end
end
