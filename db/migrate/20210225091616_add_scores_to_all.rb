# frozen_string_literal: true

class AddScoresToAll < ActiveRecord::Migration[6.1]
  def change
    add_column :tlsconfigs, :score, :integer
    add_column :dnsrrs, :score, :integer
    add_column :headers, :score, :integer
    add_column :whoisrecords, :score, :integer
  end
end
