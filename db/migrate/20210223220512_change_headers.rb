# frozen_string_literal: true

class ChangeHeaders < ActiveRecord::Migration[6.1]
  def change
    remove_column :headers, :name, :string
    remove_column :headers, :value, :string
    remove_column :headers, :secure, :boolean
    add_column :headers, :csp, :string
    add_column :headers, :hsts, :string
    add_column :headers, :https_redirect, :string
    add_column :headers, :referrer, :string
    add_column :headers, :server, :string
    add_column :headers, :xcto, :string
    add_column :headers, :xfo, :string
    add_column :headers, :xxss, :string
  end
end
