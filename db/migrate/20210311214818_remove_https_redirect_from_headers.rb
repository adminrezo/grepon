# frozen_string_literal: true

class RemoveHttpsRedirectFromHeaders < ActiveRecord::Migration[6.1]
  def change
    remove_column :headers, :https_redirect, :string
  end
end
