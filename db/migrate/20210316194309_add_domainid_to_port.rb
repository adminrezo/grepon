# frozen_string_literal: true

class AddDomainidToPort < ActiveRecord::Migration[6.1]
  def change
    add_column :ports, :domain_id, :domain
  end
end
