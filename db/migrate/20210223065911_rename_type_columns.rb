# frozen_string_literal: true

class RenameTypeColumns < ActiveRecord::Migration[6.1]
  def change
    rename_column :headers, :type, :name
    rename_column :dnsrrs, :type, :dnstype
  end
end
