# frozen_string_literal: true

class ChangeHeadersToMany < ActiveRecord::Migration[6.1]
  def change
    add_column :headers, :name, :string
    add_column :headers, :value, :string
    remove_column :headers, :csp, :string
    remove_column :headers, :hsts, :string
    remove_column :headers, :http_redirect, :string
    remove_column :headers, :referrer, :string
    remove_column :headers, :server, :string
    remove_column :headers, :xcto, :string
    remove_column :headers, :xfo, :string
    remove_column :headers, :xxss, :string
    remove_column :headers, :csp_score, :integer
    remove_column :headers, :hsts_score, :integer
    remove_column :headers, :https_score, :integer
    remove_column :headers, :referrer_score, :integer
    remove_column :headers, :server_score, :integer
    remove_column :headers, :xcto_score, :integer
    remove_column :headers, :xfo_score, :integer
    remove_column :headers, :xxss_score, :integer
  end
end
