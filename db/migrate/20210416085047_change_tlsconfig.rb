# frozen_string_literal: true

class ChangeTlsconfig < ActiveRecord::Migration[6.1]
  def change
    remove_column :tlsconfigs, :sslv3, :boolean
    remove_column :tlsconfigs, :tlsv10, :boolean
    remove_column :tlsconfigs, :tlsv11, :boolean
    remove_column :tlsconfigs, :tlsv12, :boolean
    remove_column :tlsconfigs, :sslv3_score, :integer
    remove_column :tlsconfigs, :tlsv10_score, :integer
    remove_column :tlsconfigs, :tlsv11_score, :integer
    remove_column :tlsconfigs, :tlsv12_score, :integer
    remove_column :tlsconfigs, :score, :float
    add_column :tlsconfigs, :version, :string
    add_column :tlsconfigs, :isused, :boolean
  end
end
