# frozen_string_literal: true

class CreateTlsconfigs < ActiveRecord::Migration[6.1]
  def change
    create_table :tlsconfigs do |t|
      t.string :protocol
      t.float :version

      t.timestamps
    end
  end
end
