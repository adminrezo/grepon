# frozen_string_literal: true

class CreateMailconfigs < ActiveRecord::Migration[6.1]
  def change
    create_table :mailconfigs do |t|
      t.string :mx
      t.string :spf
      t.string :dkim
      t.string :dmarc
      t.integer :score
      t.belongs_to :domain

      t.timestamps
    end
  end
end
