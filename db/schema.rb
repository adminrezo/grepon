# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_04_27_210013) do

# Could not dump table "dnsrrs" because of following StandardError
#   Unknown type '' for column 'domain_id'

# Could not dump table "domains" because of following StandardError
#   Unknown type '' for column 'user_id'

# Could not dump table "headers" because of following StandardError
#   Unknown type '' for column 'domain_id'

  create_table "passwords", force: :cascade do |t|
    t.integer "length"
    t.boolean "lower"
    t.boolean "upper"
    t.boolean "numbers"
    t.boolean "specials"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

# Could not dump table "ports" because of following StandardError
#   Unknown type 'domain' for column 'domain_id'

# Could not dump table "tlsconfigs" because of following StandardError
#   Unknown type '' for column 'domain_id'

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

# Could not dump table "whoisrecords" because of following StandardError
#   Unknown type '' for column 'domain_id'

end
