# Grepon

![Pipeline Status](https://gitlab.com/adminrezo/grepon/badges/master/pipeline.svg)

Grépon aims to quickly show the security status of a Website.

It also comes with a few tools.

![Grépon](public/images/grepon-on-readme.jpg)

Photo by <a href="https://unsplash.com/@patrick_janser">Patrick Janser</a> on <a href="https://unsplash.com/s/photos/gr%C3%A9pon">Unsplash</a>

## Requirements

* Ruby version : >= 2.5
* System dependencies :
  - NodeJS <14 & NPM
  - Yarn
  - Webpack

## Test in development

Install and start mailcatcher for Devise integration :

```
gem install mailcatcher
mailcatcher
rails server
```

## Deploy in production

### Manually

#### Configure Devise

* Set `config.action_mailer.*` in `config/environments/production.rb`
* Set `config.mailer_sender` in `config/initializers/devise.rb`

#### Run the bundle

```
git clone https://gitlab.com/adminrezo/grepon
cd grepon
export RAILS_ENV=production
export EDITOR=vi
rm config/credentials.yml.enc
bin/rails credentials:edit
bundle install
bundle exec rake assets:precompile
bundle exec rake assets:clean
bundle exec rake db:migrate
bin/rails s
```

Then go to <http://127.0.0.1:3000>.

Please use it behind a reverse proxy like Apache or Nginx.


### With Docker and Docker Compose

* Set `config.action_mailer.*` in `config/environments/production.rb`
* Set `config.mailer_sender` in `config/initializers/devise.rb`
* Build and run docker

```
docker-compose up -d
```


