# frozen_string_literal: true

class ApplicationMailer < ActionMailer::Base
  default from: 'no-reply@grepon.domain.example'
  layout 'mailer'
end
