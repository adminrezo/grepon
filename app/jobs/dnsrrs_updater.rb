# frozen_string_literal: true

# Update DNS records
class DnsrrsUpdater
  def initialize(domain)
    @domain = domain
  end

  def refresh
    clean
    create
  end

  def clean
    @dns = @domain.dnsrrs
    @dns.each(&:delete)
  end

  def create
    create_std_rrs
    create_ext_rrs
    create_dmarc
    create_spf
  end

  private

  def create_std_rrs
    nstypes = %w[A AAAA CAA CNAME NS]
    nstypes.each do |type|
      rs = Dnsruby::DNS.new
      rs.each_resource(@domain.fqdn, type) do |rr|
        Dnsrr.create(dnstype: type, value: rr.rdata, domain_id: @domain.id)
      end
    end
  end

  def create_ext_rrs
    %w[MX SRV TXT].each do |type|
      rs = Dnsruby::DNS.new
      rs.each_resource(@domain.fqdn, type) do |rr|
        value = ''
        rr.rdata.each do |r|
          value += "#{r} "
        end
        Dnsrr.create(dnstype: type, value: value, domain_id: @domain.id)
      end
    end
  end

  def create_dmarc
    value = ''
    begin
      Dnsruby::DNS.new.each_resource("_dmarc.#{@domain.fqdn}", 'TXT') do |record|
        record.rdata.each do |data|
          value += "#{data} / "
        end
        Dnsrr.create(dnstype: 'DMARC', value: value, domain_id: @domain.id)
      end
    rescue StandardError
      Dnsrr.create(dnstype: 'DMARC', value: '', domain_id: @domain.id)
    end
  end

  def create_spf
    value = ''
    begin
      Dnsruby::DNS.new.each_resource(@domain.fqdn, 'TXT') do |rr|
        rr.rdata.each do |r|
          value += "#{r} / " if r.include?('spf')
        end
        Dnsrr.create(dnstype: 'SPF', value: value, domain_id: @domain.id)
      end
    rescue StandardError
      Dnsrr.create(dnstype: 'SPF', value: '', domain_id: @domain.id)
    end
  end
end
