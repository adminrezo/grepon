# frozen_string_literal: true

# Update domains globally
class DomainUpdater
  def initialize(domain)
    @domain = domain
    @list = [DnsrrsUpdater, HeadersUpdater, PortsUpdater, TlsconfigUpdater, WhoisUpdater]
  end

  def destroy
    @domain.destroy
  end

  def refresh
    clean_all
    request_all
  end

  private

  def clean_all
    @list.each do |upd|
      upd.new(@domain)
      upd.clean
    end
  end

  def request_all
    @list.each do |upd|
      upd.new(@domain)
      upd.create
    end
  end
end
