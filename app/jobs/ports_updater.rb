# frozen_string_literal: true

# Update ports statuses
class PortsUpdater
  def initialize(domain)
    @domain = domain
  end

  def refresh
    clean
    create
  end

  def clean
    @ports = @domain.ports
    @ports.each(&:delete)
  end

  def create
    list = [21, 22, 23, 25, 53, 80, 110, 111, 135, 139, 143, 443, 445, 993, 995, 1723, 3306, 3389, 5900, 8080]
    list.each do |port|
      isopen = true
      begin
        TCPSocket.open(@domain.fqdn, port, connect_timeout: 0.5)
      rescue StandardError
        isopen = false
      end
      Port.create(number: port, protocol: 'TCP', opened: isopen, domain_id: @domain.id)
    end
  end
end
