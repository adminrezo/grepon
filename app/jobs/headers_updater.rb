# frozen_string_literal: true

# Update headers from a webpage
class HeadersUpdater
  def initialize(domain)
    @domain = domain
    @seclist = { 'referrer-policy' => %w[no-referrer same-origin strict-origin strict-origin-when-cross-origin],
                 'set-cookie' => %w[secure httponly expires max-age],
                 'x-aspnet-version' => false,
                 'x-content-type-options' => 'nosniff',
                 'x-frame-options' => %w[sameorigin deny],
                 'x-powered-by' => false,
                 'x-xss-protection' => '1; mode=block' }
  end

  def refresh
    clean
    create
  end

  def clean
    set_headers
    @headers.each(&:delete)
  end

  def create
    set_response
    @rsp&.each_key do |k|
      v = @rsp[k]
      s = secure?(k, v)
      Header.create(name: k, value: v, issecure: s, domain_id: @domain.id)
    end
  end

  private

  def set_headers
    @headers = @domain.headers
  end

  def set_response
    @rsp = begin
      HTTParty.head("http://#{@domain.fqdn}", timeout: 3)
    rescue StandardError
      @rsp = nil
    end
  end

  def secure?(header, value)
    rsp = true
    value = value.downcase
    rsp = false unless @seclist[header] != value
    rsp
  end

  def test_referrer
    rsp = true
    rsp = false if value != ('no-referrer' || 'same-origin' || 'strict-origin' || 'strict-origin-when-cross-origin')
    rsp
  end

  def test_cookie
    rsp = true
    rsp = false unless value.include?('secure')
    rsp = false unless value.include?('httponly')
    rsp = false if !value.include?('expires') || !value.include?('max-age')
    rsp
  end

  def test_sts
    rsp = true
    value = value.split('max-age=')[1]
    value = value.split(';')[0].to_i
    rsp = false if value < 15_768_000
    rsp
  end
end
