# frozen_string_literal: true

class Domain < ApplicationRecord
  belongs_to :user
  has_many :dnsrrs, dependent: :destroy
  has_many :headers, dependent: :destroy
  has_many :ports, dependent: :destroy
  has_many :tlsconfigs, dependent: :destroy
  has_one :whoisrecord, dependent: :destroy
  validates :fqdn, format: { with: /\A[a-z0-9][a-z0-9.-]+[a-z]\z/, message: 'Domain names only' }
end
