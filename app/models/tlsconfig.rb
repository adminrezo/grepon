# frozen_string_literal: true

class Tlsconfig < ApplicationRecord
  belongs_to :domain
  validates :version, inclusion: { in: %w[SSLv3 TLSv1 TLSv1_1 TLSv1_2] }
end
