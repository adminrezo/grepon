# frozen_string_literal: true

class Dnsrr < ApplicationRecord
  belongs_to :domain
end
