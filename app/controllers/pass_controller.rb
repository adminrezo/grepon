# frozen_string_literal: true

# Password generator
class PassController < ApplicationController
  # GET /tools/password
  def index; end

  # POST /tools/password
  def show
    gen_pass
  end

  private

  def gen_pass
    pw = Password.new(length: params[:len],
                      lower: params[:low],
                      upper: params[:upp],
                      numbers: params[:num],
                      specials: params[:spec])
    @pass = PasswordGenerator.gen(pw)
  end
end
