# frozen_string_literal: true

# TLS Protocols enabled
class TlsconfigsController < GreponController
  before_action :set_domain

  # GET /tls/update/1
  def update
    tu = TlsconfigUpdater.new(@domain)
    tu.clean
    tu.create
    redirect_to action: 'show', id: @domain.id
  end

  # GET /tls/1
  def show
    set_tls
    set_lastupdate
  end

  private

  def set_domain
    @domain = Domain.find(params[:id])
  end

  def set_tls
    @tls = Tlsconfig.where(domain_id: @domain.id)
  end

  def set_lastupdate
    @lastupdate = if @tls.first
                    @tls.first.updated_at.to_formatted_s(:short)
                  else
                    'Never'
                  end
  end
end
