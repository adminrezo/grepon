# frozen_string_literal: true

# Headers controller
class HeadersController < GreponController
  before_action :set_domain

  # GET /header/update/1
  def update
    hu = HeadersUpdater.new(@domain)
    hu.clean
    hu.create
    redirect_to action: 'show', id: @domain.id
  end

  # GET /header/1
  def show
    set_headers
    set_lastupdate
  end

  private

  def set_domain
    @domain = Domain.find(params[:id])
  end

  def set_headers
    @headers = @domain.headers.order('name asc')
  end

  def set_lastupdate
    @lastupdate = if @headers.first
                    @headers.first.updated_at.to_formatted_s(:short)
                  else
                    'Never'
                  end
  end
end
