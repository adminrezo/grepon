# frozen_string_literal: true

# DNS Records
class DnsrrsController < GreponController
  before_action :set_domain

  # GET /dns/update/1
  def update
    du = DnsrrsUpdater.new(@domain)
    du.refresh
    redirect_to action: 'show', id: @domain.id
  end

  # GET /dns/1
  def show
    set_rrs
    set_lastupdate
  end

  private

  def set_domain
    @domain = Domain.find(params[:id])
  end

  def set_rrs
    @rrs = @domain.dnsrrs
  end

  def set_lastupdate
    @lastupdate = if @rrs.first
                    @rrs.first.updated_at.to_formatted_s(:short)
                  else
                    'Never'
                  end
  end
end
