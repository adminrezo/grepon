# frozen_string_literal: true

# Mini Port scanner
class PortsController < GreponController
  before_action :set_domain

  # GET /port/update/1
  def update
    pu = PortsUpdater.new(@domain)
    pu.clean
    pu.create
    redirect_to action: 'show', id: @domain.id
  end

  # GET /port/1
  def show
    set_ports
    set_lastupdate
  end

  private

  def set_domain
    @domain = Domain.find(params[:id])
  end

  def set_ports
    @ports = Port.where(['domain_id = ? and opened = ?', @domain.id, true])
  end

  def set_lastupdate
    @lastupdate = if @ports.first
                    @ports.first.updated_at.to_formatted_s(:short)
                  else
                    'Never'
                  end
  end
end
