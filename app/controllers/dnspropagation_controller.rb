# frozen_string_literal: true

# DNS Propagation
class DnspropagationController < ApplicationController
  # GET /tools/dnspropagation
  def index; end

  # POST /tools/dnspropagation
  def show
    @fqdn = params[:fqdn]
    dp = DnsPropagation.new(@fqdn)
    @hash = dp.get.to_a
  end
end
