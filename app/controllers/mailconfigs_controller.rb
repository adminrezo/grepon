# frozen_string_literal: true

# Mail configuration
class MailconfigsController < GreponController
  before_action :set_domain

  # GET /mail/update/1
  def update
    du = DnsrrsUpdater.new(@domain)
    du.clean
    du.create
    redirect_to action: 'show', id: @domain.id
  end

  # GET /mail/1
  def show
    set_dns
    set_lastupdate
  end

  private

  def set_domain
    @domain = Domain.find(params[:id])
  end

  def set_mailconfig
    @mail = Mailconfig.find_by(domain_id: @domain.id)
    @mail = Mailconfig.create(domain_id: @domain.id) if @mail.nil?
  end

  def set_dns
    result = {}
    %w[MX SPF DMARC].each do |type|
      res = ''
      @domain.dnsrrs.where(dnstype: type).each do |rr|
        res += " #{rr.value}"
      end
      result[type] = res
    end
    @dns = result
  end

  def set_lastupdate
    @lastupdate = if @mail
                    @mail.updated_at.to_formatted_s(:short)
                  else
                    'Never'
                  end
  end
end
