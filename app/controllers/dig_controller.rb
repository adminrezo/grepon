# frozen_string_literal: true

# Web Dig
class DigController < ApplicationController
  # GET /tools/dig
  def index; end

  # POST /tools/dig
  def show
    fqdn = params[:fqdn]
    dnstype = %w[A AAAA CAA CNAME MX NS PTR SOA SRV TXT]
    @result = {}
    dnstype.each do |thetype|
      @result[thetype] = []
      res = Dnsruby::DNS.new(timeouts: 1)
      res.each_resource(fqdn, thetype) do |rr|
        @result[thetype].push(rr.rdata.to_s)
      end
    end
  end
end
