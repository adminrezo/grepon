# frozen_string_literal: true

class GuideController < ApplicationController
  # GET /guide/dns
  def dns; end

  # GET /guide/header
  def header; end

  # GET /guide/mail
  def mail; end

  # GET /guide/port
  def port; end

  # GET /guide/tls
  def tls; end

  # GET /guide/whois
  def whois; end
end
