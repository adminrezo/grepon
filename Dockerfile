FROM ruby:buster

ENV RAILS_ENV production
ENV EDITOR=vi

WORKDIR /home/grepon
COPY . /home/grepon
RUN bundle install
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update && apt-get install -y yarn
RUN yarn install --check-files
RUN rm config/credentials.yml.enc
RUN bin/rails credentials:edit
RUN rake db:migrate
RUN bin/rails webpacker:compile

EXPOSE 3000
CMD ["rails", "server", "-b", "0.0.0.0"]
